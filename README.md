Ansible repository for managing new/transitional mailman service.

Links:

* <https://alioth-lists.debian.net/>
* <https://wiki.debian.org/Alioth/MailingListContinuation>

# How to use this repo

Merge requests are welcome at all times. You should only merge MRs or
make commits directly to master if you are a sysadmin.

You will need to have ansible installed locally. Documentation for
how ansible works is at <http://docs.ansible.com/>. The version of
ansible to be used is 2.2.x (in Debian stable). You can use later
versions, but ensure that compatibility is maintained.

The basic model is that ansible uses ssh to communicate with the 
host being configured, in order to copy across specialised python scripts
to perform whatever function is needed. You therefore need to be running
somewhere that has access to the relevant private key. You will also
need passwordless sudo access to have been set up for you.

# Cheat sheet

`site.yml` should contain references to all the generally-used roles,
and should be idempotent and thus reflect the ongoing state of the system
(other playbooks, if any, might be used for specialised tasks). Other
playbooks (`*.yml`) generally operate on a single role, although some
roles refer to others using the `meta` directory.

In all cases, run from within the checkout:

## Output all the changes that would be performed by roles configured in the repository

    $ ansible-playbook site.yml --check --diff

## Apply those changes

    $ ansible-playbook site.yml

# Policy

* In general, do not make changes to the system configuration (roughly
defined as being any changes to /etc, or the packages installed) without
updating ansible. If for some reason this is necessary, create an issue
reflecting the divergence.
* Do not `su` to root unless absolutely needed. Prefer to use sudo so that
a history of actions, useful for later investigation, is available.
* Changes which have been used for a deployment should be pushed to the repository as close to the time they were applied as practical (either before or after).
* Discuss controversial changes in MRs or on IRC (`#alioth-lists`).

# The Mailman package

We have a locally built mailman package, see
<https://salsa.debian.org/alioth-lists-team/mailman2/-/tree/master>
We continue to maintain the Debian specific files in ansible, but anything
where we change a file in the package should be done by rebuilding the
package.

This package also includes a new upstream version compared to the
latest in Debian.

There is no repo or central build infrastructure. Use sbuild/cowbuilder,
copy over and dpkg -i.

There is a dpkg hold so automatic upgrades should not happen.

# Recaptcha

To avoid the subscription web form being abused we deployed reCAPTCHA -
management console at
<https://www.google.com/recaptcha/admin/site/442526218>
