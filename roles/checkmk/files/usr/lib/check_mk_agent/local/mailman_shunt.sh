#!/bin/sh

SHUNT=/var/lib/mailman/qfiles/shunt
WARN=1
CRIT=5

if [ ! -d ${SHUNT} ]; then
	exit 0
fi

COUNT=$( find ${SHUNT} -type f | wc -l )

echo "P Mailman_Shunt files=${COUNT};${WARN};${CRIT} ${COUNT} files in shunt queue"
