#!/usr/bin/perl

use strict;
use warnings;

use MIME::Lite;

my $list = shift or die "No list specified";
my $owner_email = $list . '-owner@lists.alioth.debian.org';

my $msg = MIME::Lite->new(
    From => 'alioth lists migration team <admin@alioth-lists.debian.net>',
    To => $owner_email,
    Subject => "Reminder: migration of alioth list $list",
    Data => <<EOF
Dear list owner,

This is a follow-up to the email we sent all alioth list owners last month;
we are now contacting list owners for lists where the desired status is
unknown.

Because there are a significant number of lists where the current owner
is not active/responding, but where other project members are requesting
migrations, we are now asking all list owners, if possible, to respond
with either a request to migrate or to leave untouched each list.
We will then follow this up with a final mail to the lists themselves,
but we would like to minimise the number of lists contacted in this way
because of the extra noise this would cause for all concerned.

The date of the migration and any other details will be announced on
debian-devel-announce nearer the time but your response within the
next two weeks would be appreciated.

More information about the new service can be found here:
<https://wiki.debian.org/Alioth/MailingListContinuation>

The text of the previous message follows:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As per the announcement on debian-devel-announce[1] the migration of
lists.alioth.debian.org mailing lists is now underway. If you would
like $list\@lists.alioth.debian.org
to be included in this process, to ensure that the list still works
after the migration in late March/April, please let us know by replying
to this email. Otherwise, the list will stop working at migration time
and the archives will no longer be accessible.

Feel free to also let us know if the list is no longer needed, so we can
note that down too. Replies must be received by 15th March 2018.

More information about the new service can be found here:
<https://wiki.debian.org/Alioth/MailingListContinuation>

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thanks,
the alioth-lists migration team.
EOF
);

$msg->send;
#$msg->print;
