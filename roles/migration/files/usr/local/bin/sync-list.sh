#!/bin/bash -e

if [ -x "$2" ]; then
    echo "Usage: $0 <SSH key> <list name>"
    exit 1
fi

RPATH=list@alioth.debian.org:/var/lib/mailman
LPATH=/var/lib/mailman

echo "Syncing list archives for $2"
rsync -ae "ssh -i $1" --delete $RPATH/archives/private/$2/ $LPATH/archives/private/$2/
rsync -ae "ssh -i $1" --delete $RPATH/archives/private/$2.mbox/ $LPATH/archives/private/$2.mbox/
rsync -ae "ssh -i $1" --delete $RPATH/archives/public/$2 $LPATH/archives/public/$2
if ssh -i $1 list@alioth.debian.org stat -t /var/lib/mailman/archives/public/$2.mbox; then
    rsync -ae "ssh -i $1" --delete --progress $RPATH/archives/public/$2.mbox $LPATH/archives/public/$2.mbox
fi

if [ `readlink $LPATH/archives/public/$2` == "/srv/lists.alioth.debian.org/archives/private/$2" ]; then
    ln -sfv $LPATH/archives/private/$2 $LPATH/archives/public/$2
fi

echo "Fixing up archive links"
find $LPATH/archives/private/$2 -type f -name 'index.html' -o -name 'author.html' -o -name 'date.html' -o -name 'subject.html' -o -name 'thread.html' | xargs -n10 perl -pi -e 's#http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/#https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/#'

echo "Syncing list config for $2"
rsync -ae "ssh -i $1" --delete $RPATH/lists/$2/ $LPATH/lists/$2/
/usr/lib/mailman/bin/withlist -l -r fix_url $2

echo Done
