#!/usr/bin/perl

use strict;
use warnings;

use MIME::Lite;

my $list = shift or die "No list specified";
my $list_email = $list . '@lists.alioth.debian.org';

my $msg = MIME::Lite->new(
    From => 'alioth lists migration team <admin@alioth-lists.debian.net>',
    To => $list_email,
    Subject => "Notice of mailing list closure: $list",
    Data => <<EOF
Dear list subscribers,

As per the announcement on debian-devel-announce[1] and as part of
the shutdown of the alioth service, the migration of
lists.alioth.debian.org mailing lists to alioth-lists.debian.net is now
underway.

We tried to contact the designated list owner via
$list-owner\@lists.alioth.debian.org but got either no reply,
or a bounce message. Accordingly, this list will not be migrated to the
new system and will stop working on 14th April.

Information about alternatives to this service which may be more suitable
for the list can be found at
<https://wiki.debian.org/Salsa/AliothMigration#Import_mailing_list>.

In the event that this list should be migrated to the new system,
please first appoint a Debian developer as a list owner, then let us know
by replying to this email, copying in the list.

More information about the new service can be found here:
<https://wiki.debian.org/Alioth/MailingListContinuation>

Thanks,
the alioth-lists migration team.

[1] <https://lists.debian.org/debian-devel-announce/2018/01/msg00003.html>


EOF
);

$msg->send;
#$msg->print;
